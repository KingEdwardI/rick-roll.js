const ffmpeg = require('fluent-ffmpeg');
const jp2a = require('jp2a');
const fs = require('fs');
const path = require('path');

// Set the path to the ffmpeg and ffprobe binaries
ffmpeg.setFfmpegPath('/usr/local/bin/ffmpeg');
ffmpeg.setFfprobePath('/usr/local/bin/ffprobe');

function splitVideoFrames() {
    // Convert video to frames
    ffmpeg('Ricky.mp4')
      .outputOptions('-vf', 'fps=1,crop=in_w-2*405:in_h-2*108')
      // ex output: frame-00001
      .output('frame%04d.jpg')
      .on('end', function() {
        console.log('Frames have been generated');
        convertFramesToAscii();
      })
      .on('error', function(err) {
        console.log('An error occurred: ' + err.message);
      })
      .run();
}

// Convert frames to ASCII
function convertFramesToAscii() {
  fs.readdir('.', (err, files) => {
    if (err) throw err;

    files.forEach(file => {
      if (path.extname(file) === '.jpg') {
        jp2a(file, (err, ascii) => {
          if (err) throw err;

          fs.writeFile(file + '.txt', ascii, err => {
            if (err) throw err;
            console.log('The file has been saved!');
          });
        });
      }
    });
  });
}

