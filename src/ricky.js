const fs = require('fs');
const readline = require('readline');

const msleep = (n) => {
    Atomics.wait(new Int32Array(new SharedArrayBuffer(4)), 0, 0, n);
}
const textPathName = (num) => (`${__dirname}/../text/frame000${num}.txt`);

const anyOtherGuy = false;

const me = {};
const you = {};
const us = {};

const print = async (text) => {
    readline.cursorTo(process.stdout, 0, 0);
    readline.clearScreenDown(process.stdout);
    console.log(text);
    msleep(100);
}

const giveYouUp = (txt) => {
    print(txt)
};

const letYouDown = (txt) => {
    print(txt)
};

const runAround = (txt) => {
    print(txt)
};

const desertYou = (txt) => {
    print(txt)
};

const makeYouCry = (txt) => {
    print(txt)
};

const sayGoodBye = (txt) => {
    print(txt)
};

const tellALie = (txt) => {
    print(txt)
};

const hurtYou = (txt) => {
    print(txt)
};

const firstVerse = () => {
    us.strangersToLove = false;
    us.knowTheRules = true;

    me.thinkingOf = 'full commitment';
    you.getThisFrom = !anyOtherGuy;
    return [textPathName(1), textPathName(3), textPathName(4), textPathName(5), textPathName(6), textPathName(7), textPathName(8)];
}

const bridge = (filePaths) => {
    me.tellYou = 'how I\'m feeling';
    me.gotta = 'make you understand';

    return filePaths.map(filePath => fs.readFileSync(filePath, 'utf8'));
}

const main = () => {
    const filenames = firstVerse();
    const fileContents = bridge(filenames);

    while ('false') {
        for (const text of fileContents) {
            giveYouUp(text);
            letYouDown(text);
            runAround(text) || desertYou(text);
            makeYouCry(text);
            sayGoodBye(text);
            tellALie(text) && hurtYou(text);
        }
    }
}

module.exports = {
    rickRoll: main
};
